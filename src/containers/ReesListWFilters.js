import React from "react";
import axios from "axios";
import Pagination from "react-js-pagination";
import FilterForm from "../components/FilterForm";
import ReesCard from "../components/ReesCard";

import "../styles/ReesListWFilters.css";

const NUM_OF_ITEMS = parseInt(process.env.REACT_APP_NUM_OF_ITEMS);
const NUM_OF_ROWS = 4;

class ReesListWFilters extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
      totalItems: 0,
      cityId: 1,
      districtId: 11,
      reesTypeId: 2,
      transactionId: 1,
      posts: [],
    };
  }

  getPosts = (offset) => {
    let json = {
      address_city: this.state.cityId,
      address_district: this.state.districtId,
      realestate_type: this.state.reesTypeId,
      transaction_type: this.state.transactionId,
      limit: NUM_OF_ITEMS * NUM_OF_ROWS,
      offset: offset,
    };
    let apiUrl = `${
      process.env.REACT_APP_API_GET_POST_BY_FILTER
    }${JSON.stringify(json)}`;
    axios
      .get(apiUrl)
      .then((response) => this.setState({ posts: response.data.data }));
  };

  onPageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getPosts((pageNumber - 1) * NUM_OF_ITEMS * NUM_OF_ROWS);
  };

  getNumOfPosts = () => {
    let json = {
      address_city: this.state.cityId,
      address_district: this.state.districtId,
      realestate_type: this.state.reesTypeId,
      transaction_type: this.state.transactionId,
    };
    let apiUrl = `${
      process.env.REACT_APP_API_GET_TOTAL_NUM_OF_FILTERED_POSTS
    }${JSON.stringify(json)}`;
    axios
      .get(apiUrl)
      .then((response) =>
        this.setState({ totalItems: response.data.data.num_of_posts })
      );
  };

  componentDidMount = () => {
    this.getNumOfPosts();
    setTimeout(() => this.getPosts(0), 100);
  };

  onFinish = (values) => {
    this.setState({
      showResult: true,
      districtId: values.district,
      cityId: values.city,
      reesTypeId: values.realEstateType,
      transactionId: values.realEstateTransType,
    });
    this.getNumOfPosts();
    this.getPosts((this.state.activePage - 1) * NUM_OF_ITEMS * NUM_OF_ROWS);
  };
  render() {
    return (
      <div className="rees-detail-list">
        <div
          classBame="reest-detail-list-filter"
          style={{ paddingTop: "14px", float: "right" }}
        >
          <FilterForm
            styles={{
              width: "15%",
              margin: "auto",
              padding: "20px 10px 10px 10px",
              position: "absolute",
              top: "10%",
              right: "10%",
              border: "solid 1px tomato",
            }}
            span={12}
            cityId={this.state.cityId}
            updateCityId={(value) => this.setState({ cityId: value })}
            updateDistrictId={(value) => this.setState({ districtId: value })}
            updateReesType={(value) => this.setState({ reesTypeId: value })}
            updateTransType={(value) => this.setState({ transactionId: value })}
            onFinish={(values) => this.onFinish(values)}
          />
        </div>
        <div
          className="list-rees-card"
          style={{
            width: "70%",
            float: "left",
            paddingTop: "24px",
            paddingLeft: "10%",
          }}
        >
          <p>Có tất cả {this.state.totalItems} kết quả.</p>
          <ReesCard gutter={[16, 24]} span={6} posts={this.state.posts} />
        </div>
        <div style={{ clear: "both" }}></div>
        <div className="paging" style={{ paddingBottom: "8px" }}>
          <Pagination
            itemClass="page-item"
            linkClass="page-link"
            activePage={this.state.activePage}
            itemsCountPerPage={NUM_OF_ITEMS}
            totalItemsCount={parseInt(
              this.state.totalItems / (NUM_OF_ITEMS * NUM_OF_ROWS)
            )}
            pageRangeDisplayed={10}
            onChange={this.onPageChange}
          />
        </div>
      </div>
    );
  }
}

export default ReesListWFilters;
