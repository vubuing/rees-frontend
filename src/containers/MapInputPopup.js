import React from "react";
import axios from "axios";
import { Modal } from "react-responsive-modal";
import { Form, Row, Col, Card, Collapse } from "antd";
// import { Divider } from "antd";
import {
  AddressNumber,
  AddressStreet,
  Ward,
  District,
  City,
  RealEstateType,
  NumOfFloor,
  RealEstatePosition,
  Area,
  SubmitBtn,
} from "../components/ReesInputFormElements";
import "react-responsive-modal/styles.css";
import "../styles/MapInputPopup.css";

const { Panel } = Collapse;

class MapInputPopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addressNumber: "268",
      addressStreet: "Lý Thường Kiệt",
      ward: "14",
      districtId: 11,
      cityId: 1,
      reesTypeId: 2,
      numOfFloor: 1,
      positionId: 1,
      area: 60,
      showResult: false,
      priceFromComparison: 0,
      priceFromGeneticAlgorithms: 0,
    };
  }

  getPriceFromAPIs = () => {
    let json = {
      lat: this.props.markerInfo.lat,
      long: this.props.markerInfo.lng,
      address_number: this.state.addressNumber,
      address_street: this.state.addressStreet,
      address_ward: this.state.ward,
      address_district: this.state.districtId,
      address_city: this.state.cityId,
      floor: this.state.numOfFloor,
      position: this.state.positionId,
      area: this.state.area,
      min_distance: process.env.REACT_APP_MIN_DISTANCE,
      max_distance: process.env.REACT_APP_MAX_DISTANCE,
    };
    let apiUrl = `${
      process.env.REACT_APP_API_CAL_PRICE_BY_COMPARISON
    }${JSON.stringify(json)}`;
    axios(apiUrl).then((response) => {
      if (response.data.price > 0) {
        this.setState({
          priceFromComparison: response.data.price,
        });
      }
    });

    let apiUrlGA = `${
      process.env.REACT_APP_API_CAL_PRICE_BY_GA
    }${JSON.stringify(json)}`;
    axios(apiUrlGA).then((response) => {
      if (response.data.price > 0) {
        this.setState({
          priceFromGeneticAlgorithms: response.data.price,
        });
      }
    });
    console.log(json);
  };

  onFinish = (values) => {
    console.log("Received values of form: ", values);
    this.setState({
      showResult: true,
      addressNumber: values.addressNumber,
      addressStreet: values.addressStreet,
      ward: values.ward,
      districtId: values.district,
      cityId: values.city,
      reesTypeId: values.realEstateType,
      numOfFloor: parseFloat(values.numOfFloor),
      positionId: values.realEstatePosition,
      area: parseFloat(values.area),
    });
    this.getPriceFromAPIs();
  };

  render() {
    return (
      <div className="map-input-popup">
        <Modal
          open={this.props.isInputTable}
          onClose={this.props.closeInputTable}
          closeIconSize={14}
          center
        >
          <Collapse defaultActiveKey={["1"]} accordion>
            <Panel header="Thông tin bất động sản" key="1">
              {/* <Divider>Tính giá bất động sản</Divider> */}
              <Form
                {...formItemLayout}
                ref={formRef}
                name="register"
                onFinish={this.onFinish}
                scrollToFirstError
              >
                <Row>
                  <Col span={12}>
                    <AddressNumber
                      update={(value) =>
                        this.setState({ addressNumber: value })
                      }
                    />
                  </Col>
                  <Col span={12}>
                    <AddressStreet
                      update={(value) =>
                        this.setState({ addressStreet: value })
                      }
                    />
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <Ward update={(value) => this.setState({ ward: value })} />
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <District
                      label={"Quận/Huyện"}
                      placeholder={"Chọn"}
                      cityId={this.state.cityId}
                      update={(value) => this.setState({ districtId: value })}
                    />
                  </Col>
                  <Col span={12}>
                    <City
                      label={"Tỉnh/Thành"}
                      placeholder={"Chọn"}
                      update={(value) => this.setState({ cityId: value })}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <RealEstateType
                      label={"Loại BĐS"}
                      placeholder={"Chọn"}
                      update={(value) => this.setState({ reesTypeId: value })}
                    />
                  </Col>
                  <Col span={12}>
                    <NumOfFloor
                      update={(value) => this.setState({ numOfFloor: value })}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col span={12}>
                    <RealEstatePosition
                      update={(value) => this.setState({ positionId: value })}
                    />
                  </Col>
                  <Col span={12}>
                    <Area update={(value) => this.setState({ area: value })} />
                  </Col>
                </Row>
                <Form.Item {...tailFormItemLayout}>
                  <SubmitBtn />
                </Form.Item>
              </Form>
            </Panel>
            <Panel header="Kết quả" key="2">
              {this.state.showResult && (
                <div>
                  {/* <Divider>Kết quả</Divider> */}
                  <div className="result-content">
                    <Row gutter={16}>
                      <Col span={12}>
                        <Card title="Phương pháp so sánh giá" loading={false}>
                          <p>
                            <strong>
                              Giá của bất động sản:{" "}
                              <span style={{ color: "tomato" }}>
                                {this.state.priceFromComparison === 0
                                  ? "Không tìm được giá cho bất động sản này."
                                  : this.state.priceFromComparison
                                      .toFixed(3)
                                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              </span>
                            </strong>
                          </p>
                          <p>
                            Giá này được tham khảo dựa trên các bất động sản
                            xung quanh có các điểm tương đồng.
                          </p>
                        </Card>
                      </Col>
                      <Col span={12}>
                        <Card title="Phương Pháp Chọn Lọc" loading={false}>
                          <p>
                            <strong>
                              Giá của bất động sản:{" "}
                              <span style={{ color: "tomato" }}>
                                {this.state.priceFromGeneticAlgorithms === 0
                                  ? "Không tìm được giá cho bất động sản này."
                                  : this.state.priceFromGeneticAlgorithms
                                      .toFixed(3)
                                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                              </span>
                            </strong>
                          </p>
                        </Card>
                      </Col>
                    </Row>
                  </div>
                </div>
              )}
            </Panel>
          </Collapse>
        </Modal>
      </div>
    );
  }
}

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const formRef = React.createRef();

export default MapInputPopup;
