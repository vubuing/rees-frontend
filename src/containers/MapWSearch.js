import React from "react";
import { Map, GoogleApiWrapper, Marker, InfoWindow } from "google-maps-react";
import axios from "axios";

import "../styles/MapWSearch.css";
import LocationSearchInput from "../components/LocationSearchInput";
import ReesTitleListToggle from "./ReesTitleListToggle";
import MapInputPopup from "./MapInputPopup";

export class MapWSearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: {
        id: null,
        lat: null,
        lng: null,
      },
      center: {
        lat: 10.773438,
        lng: 106.659452,
      },
      places: [
        {
          id: null,
          lat: null,
          long: null,
          title: "",
          content: "",
        },
      ],
      subPlaces: [{ id: null, lat: null, long: null, title: "", content: "" }],
      mapBounds: [
        { lat: null, lng: null }, // North-East Coordinate Point
        { lat: null, lng: null }, // South-West Coordinate Point
      ],
      placesReady: false,
      isInputTable: false,
      mapProps: null,
      mapParam: null,
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
    };
  }

  shouldComponentUpdate = (nextState) => {
    return this.state.subPlaces !== nextState.subPlaces;
  };

  //////////////////////////////////////////////////////////////////////////////
  // Map Actions
  //////////////////////////////////////////////////////////////////////////////
  getPlacesFromAPI = () => {
    let json = {
      neLat: this.state.mapBounds[0].lat,
      neLng: this.state.mapBounds[0].lng,
      swLat: this.state.mapBounds[1].lat,
      swLng: this.state.mapBounds[1].lng,
      limit: 100,
    };
    let apiUrl = `${
      process.env.REACT_APP_API_GET_POSTS_WITHIN_AREA
    }${JSON.stringify(json)}`;
    axios.get(apiUrl).then((response) => {
      this.setState({
        placesReady: true,
        places: response.data.data,
        subPlaces: response.data.data.slice(
          0,
          parseInt(process.env.REACT_APP_NUM_OF_ITEMS)
        ),
      });
    });
  };

  fetchPlaces = (mapProps, map) => {
    // Step 1: Get map bounds
    // Step 2: Get North-East, South-West coordinates
    // Step 3: Inject coordinates into API to get places
    this.setState({ mapProps: mapProps, mapParam: map });
    setTimeout(() => {
      let bounds = map.getBounds();
      if (bounds != null) {
        let ne = {
          lat: bounds.getNorthEast().lat(),
          lng: bounds.getNorthEast().lng(),
        };
        let sw = {
          lat: bounds.getSouthWest().lat(),
          lng: bounds.getSouthWest().lng(),
        };
        this.setState({ mapBounds: [ne, sw] });
        // console.log("North-East: ", ne.lat, ne.lng);
        // console.log("South-West: ", sw.lat, sw.lng);
      }
      this.getPlacesFromAPI();
    }, 100);
  };

  onMapClicked = (props, map, clickEvent) => {
    let radId = Math.floor(Math.random() * 300000) + 3000000;
    this.setState({
      selected: {
        id: radId,
        lat: clickEvent.latLng.lat(),
        lng: clickEvent.latLng.lng(),
      },
      showingInfoWindow: false,
      activeMarker: null,
      isInputTable: false,
    });
  };

  centerMoved = (mapProps, map) => {
    this.setState({ center: map.center });
    this.fetchPlaces(mapProps, map);
  };

  //////////////////////////////////////////////////////////////////////////////
  // Marker Actions
  //////////////////////////////////////////////////////////////////////////////
  onSlectedMarkerClick = (props, marker, event) => {
    this.setState({ isInputTable: true });
  };

  onMarkerClicked = (props, marker, event, place) => {
    this.setState({
      selectedPlace: place,
      activeMarker: marker,
      showingInfoWindow: true,
      selected: { lat: null, lng: null },
    });
    console.log(place);
  };

  displayMarkers = () => {
    return this.state.places.map((place) => (
      <Marker
        key={place.id}
        name={place.title}
        icon={{
          url: "images/house_v2.png",
          anchor: new window.google.maps.Point(32, 32),
          scaledSize: new window.google.maps.Size(32, 32),
        }}
        position={{ lat: place.lat, lng: place.long }}
        onClick={(props, marker, event) =>
          this.onMarkerClicked(props, marker, event, place)
        }
      />
    ));
  };

  //////////////////////////////////////////////////////////////////////////////
  // Input Search Location
  //////////////////////////////////////////////////////////////////////////////
  updateFromLocationSearch = (latLng) => {
    this.setState({
      center: latLng,
      selected: latLng,
    });
    this.fetchPlaces(this.state.mapProps, this.state.mapParam);
  };

  //////////////////////////////////////////////////////////////////////////////
  // List Of Realestates
  //////////////////////////////////////////////////////////////////////////////
  onReesListPageChange = (numOfItems, numOfPage) => {
    this.setState({
      subPlaces: this.state.places.slice(
        numOfItems * (numOfPage - 1),
        numOfItems * numOfPage
      ),
    });
  };

  render() {
    return (
      <div className="map-container" style={{ height: "93vh", width: "100%" }}>
        <Map
          className="the-map"
          style={{ height: "93vh" }}
          google={this.props.google}
          zoom={15}
          initialCenter={this.state.center}
          center={this.state.center}
          onDragend={this.centerMoved}
          onReady={this.fetchPlaces}
          onClick={this.onMapClicked}
          onZoom_changed={this.fetchPlaces}
        >
          <Marker
            position={{
              lat: this.state.selected.lat,
              lng: this.state.selected.lng,
            }}
            onClick={this.onSlectedMarkerClick}
          />
          {this.displayMarkers()}
          <InfoWindow
            marker={this.state.activeMarker}
            visible={this.state.showingInfoWindow}
          >
            <div>
              <h2>{this.state.selectedPlace.title}</h2>
              <p style={{ fontSize: "14px" }}>
                {this.state.selectedPlace.content}
              </p>
            </div>
          </InfoWindow>
          <div className="search-box-container">
            <LocationSearchInput
              updateFromLocationSearch={this.updateFromLocationSearch}
            />
          </div>
        </Map>
        <MapInputPopup
          markerInfo={this.state.selected}
          isInputTable={this.state.isInputTable}
          closeInputTable={() => this.setState({ isInputTable: false })}
        />
        <ReesTitleListToggle
          places={this.state.subPlaces}
          totalItems={this.state.places.length}
          placesReady={this.state.placesReady}
          onReesListPageChange={this.onReesListPageChange}
        />
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: process.env.REACT_APP_GG_MAP_API_KEY,
})(MapWSearch);
