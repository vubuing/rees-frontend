import React from "react";
import axios from "axios";
import { Button } from "antd";
import ReesCard from "../components/ReesCard";
import FilterBar from "../components/FilterBar";

import "../styles/HomePage.css";

class Homepage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cityId: 1,
      districtId: 11,
      reesTypeId: 2,
      transactionId: 1,
      posts: [],
      cities: [],
      districts: [],
      reesTypes: [],
      transactions: [],
      cityIsSelected: false,
      districtIsSelected: false,
      reesTypeIsSelected: false,
      transIsSelected: false,
    };
  }

  getPosts = async () => {
    let json = {
      address_city: this.state.cityId,
      address_district: this.state.districtId,
      realestate_type: this.state.reesTypeId,
      transaction_type: this.state.transactionId,
      limit: 8,
      offset: 0,
    };
    let apiUrl = `${
      process.env.REACT_APP_API_GET_POST_BY_FILTER
    }${JSON.stringify(json)}`;
    await axios.get(apiUrl).then((response) => {
      this.setState({ posts: response.data.data });
    });
  };

  getCities = async () => {
    let apiUrl = `${process.env.REACT_APP_API_GET_CITIES}1`;
    await axios.get(apiUrl).then((reponse) => {
      this.setState({ cities: [reponse.data.data] });
    });
  };

  getDistricts = () => {
    let apiUrl = `${process.env.REACT_APP_API_GET_DISTRICTS_WITHIN_CITY}${this.state.cityId}`;
    axios.get(apiUrl).then((response) => {
      this.setState({ districts: response.data.data });
    });
  };

  getReesTypes = () => {
    let apiUrl = `${process.env.REACT_APP_API_GET_REALESTATE_TYPES}`;
    axios.get(apiUrl).then((response) => {
      this.setState({ reesTypes: response.data.data.slice(0, 2) });
    });
  };

  getTransactions = () => {
    let apiUrl = `${process.env.REACT_APP_API_GET_TRANSACTIONS}`;
    axios.get(apiUrl).then((response) => {
      this.setState({ transactions: response.data.data.slice(0, 2) });
    });
  };

  updateCityId = (id) => {
    this.setState({ cityId: id, cityIsSelected: true });
    this.getDistricts();
  };

  updateDistrictId = (id) => {
    this.setState({
      districtId: id,
      districtIsSelected: true,
    });
  };

  updateReesTypeId = (id) => {
    this.setState({
      reesTypeId: id,
      reesTypeIsSelected: true,
    });
  };

  updateTransactionId = (id) => {
    this.setState({
      transactionId: id,
      transIsSelected: true,
    });
  };

  search = () => {
    let valid =
      this.state.cityIsSelected === true &&
      this.state.districtIsSelected === true &&
      this.state.reesTypeIsSelected === true &&
      this.state.transIsSelected === true;
    if (valid) {
      this.getPosts();
    }
  };

  componentDidMount = () => {
    this.getPosts();
    this.getCities();
    this.getReesTypes();
    this.getTransactions();
  };

  render() {
    return (
      <div className="home-page">
        <div className="banner">
          <div className="banner-txt">
            <FilterBar
              cities={this.state.cities}
              updateCityId={this.updateCityId}
              getCities={this.getCities}
              districts={this.state.districts}
              updateDistrictId={this.updateDistrictId}
              reesTypes={this.state.reesTypes}
              updateReesTypeId={this.updateReesTypeId}
              transactions={this.state.transactions}
              updateTransactionId={this.updateTransactionId}
              search={this.search}
              gutter={24}
              span={6}
              style={{ margin: "20px auto" }}
            />
          </div>
        </div>
        <div className="list-rees-card">
          <ReesCard gutter={[16, 24]} span={6} posts={this.state.posts} />
        </div>
        <div className="more-rees-btn" style={{ paddingBottom: "8px" }}>
          <Button
            href="/danh-sach-bat-dong-san"
            style={{ marginLeft: "46.5%" }}
            danger
          >
            Xem Thêm
          </Button>
        </div>
      </div>
    );
  }
}

export default Homepage;
