import React from "react";
import { Button } from "antd";
import ReesTitle from "../components/ReesTitle";

class ReesList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: false,
      buttonName: "Hiện D/Sách BĐS",
    };
  }

  onButtonClicked = () => {
    if (!this.state.toggle) {
      this.setState({
        toggle: true,
        buttonName: "Ẩn Danh Sách",
      });
    } else {
      this.setState({
        toggle: false,
        buttonName: "Hiện D/Sách BĐS",
      });
    }
  };

  render() {
    return (
      <div className="list-rees-with-toggle-btn">
        <div className="list-toggle">
          <Button
            type="primary"
            style={{
              borderRadius: "15px",
              backgroundColor: "tomato",
              margin: "auto",
            }}
            onClick={this.onButtonClicked}
          >
            {this.state.buttonName}
          </Button>
        </div>
        {this.state.toggle && (
          <ReesTitle
            places={this.props.places}
            totalItems={this.props.totalItems}
            placesReady={this.props.placesReady}
            onPageChange={this.props.onReesListPageChange}
          />
        )}
      </div>
    );
  }
}

export default ReesList;
