import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "../store/actions/auth";
import { Layout, Menu, Divider, Row, Col, Avatar } from "antd";
import { UserOutlined } from "@ant-design/icons";

const { Header, Content, Footer } = Layout;

class AppLayout extends React.Component {
  render() {
    return (
      <Layout className="layout">
        <Header style={{ padding: 0 }}>
          <Menu theme="light" mode="horizontal">
            <Menu.Item key="0">
              <Link to="/">
                <img
                  src="/images/logo_v2.png"
                  alt="logo"
                  width="50px"
                  height="50px"
                />
              </Link>
            </Menu.Item>
            <Menu.Item key="1">
              <Link to="/">Trang chủ</Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/danh-sach-bat-dong-san">Các bất động sản</Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link to="/map">Bản đồ</Link>
            </Menu.Item>
            {this.props.isAuthenticated && (
              <Menu.Item key="4" style={{ float: "right" }}>
                <Link to="/profile">
                  <Avatar icon={<UserOutlined />} />
                </Link>
              </Menu.Item>
            )}
            {this.props.isAuthenticated ? (
              <Menu.Item
                key="5"
                style={{ float: "right" }}
                onClick={this.props.logout}
              >
                <Link to="/">Đăng xuất</Link>
              </Menu.Item>
            ) : (
              <Menu.Item key="4" style={{ float: "right" }}>
                <Link to="/login">Đăng nhập</Link>
              </Menu.Item>
            )}
          </Menu>
        </Header>

        <Content
          style={{
            borderTop: "1px solid #f0f0f0",
            borderBottom: "1px solid #f0f0f0",
          }}
        >
          <div className="site-layout-content">{this.props.children}</div>
        </Content>

        <Footer style={{ textAlign: "center" }}>
          <Row>
            <Col span={3}>
              <Link to="/">
                <img
                  src="/images/logo_v2.png"
                  alt="logo"
                  width="60px"
                  height="60px"
                />
              </Link>
            </Col>
            <Col span={18}>
              <Divider>
                <a href="/map">ReEs ©2020 Bất động sản cho mọi người</a>
              </Divider>
            </Col>
          </Row>
        </Footer>
      </Layout>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => dispatch(actions.logout()),
  };
};

export default connect(null, mapDispatchToProps)(AppLayout);
