import React from "react";
import { Form, Input, Button, Spin } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { LoadingOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "../store/actions/auth";

class Login extends React.Component {
  render() {
    const onFinish = (values) => {
      this.props.onAuth(values.username, values.password);
      this.props.history.push("/");
    };
    return (
      <div
        className="login-form"
        style={{ width: "50%", padding: "24px 0px 24px 10%" }}
      >
        {this.props.loading ? (
          <Spin indicator={antIcon} />
        ) : (
          <Form
            name="normal_login"
            className="login-form"
            onFinish={onFinish}
            style={{ maxWidth: "300px" }}
          >
            <Form.Item
              name="username"
              rules={[
                { required: true, message: "Vui lòng nhập tên tài khoản!" },
              ]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Tài khoản"
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[{ required: true, message: "Vui lòng nhập mật khẩu!" }]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Mật khẩu"
              />
            </Form.Item>

            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
              >
                Đăng nhập
              </Button>
              &nbsp;Hoặc&nbsp;<Link to="/register">Đăng ký!</Link>
            </Form.Item>
          </Form>
        )}
      </div>
    );
  }
}

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

const mapStateToProps = (state) => {
  return {
    loading: state.loading,
    error: state.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAuth: (username, password) =>
      dispatch(actions.authLogin(username, password)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
