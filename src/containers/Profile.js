import React from "react";
import { connect } from "react-redux";
import * as actions from "../store/actions/auth";
import { Row, Col, Divider, Avatar, Form, Input, Button } from "antd";
import { UserOutlined, LockOutlined, MailOutlined } from "@ant-design/icons";

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      account: JSON.parse(localStorage.getItem("account")),
    };
  }

  render() {
    const onFinish = (values) => {
      let username = values.username;
      // let email = values.email;
      let pwd = values.password;
      this.props.onAuth(username, pwd);
    };
    return (
      <div
        className="usr-profile"
        style={{ width: "50%", margin: "auto", paddingTop: "24px" }}
      >
        <Divider
          orientation="left"
          style={{ color: "#333", fontWeight: "normal" }}
        >
          <strong>Thông tin cá nhân</strong>
        </Divider>
        <Row>
          <Col flex={2}>
            <Avatar
              shape="square"
              size={128}
              icon={<UserOutlined />}
              style={{ marginLeft: "30%" }}
            />
          </Col>
          <Col flex={3}>
            <Form
              {...formItemLayout}
              name="register"
              onFinish={onFinish}
              scrollToFirstError
            >
              <Form.Item name="username" label="Tài khoản">
                <Input
                  prefix={<UserOutlined className="site-form-item-icon" />}
                  placeholder={this.state.account.username}
                />
              </Form.Item>

              <Form.Item
                name="email"
                label="E-mail"
                rules={[
                  {
                    type: "email",
                    message: "Định dạng e-mail không hợp lệ!",
                  },
                ]}
              >
                <Input
                  prefix={<MailOutlined className="site-form-item-icon" />}
                  placeholder={this.state.account.email}
                  disabled={true}
                />
              </Form.Item>

              <Form.Item name="password" label="Mật khẩu" hasFeedback>
                <Input
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  placeholder="********"
                />
              </Form.Item>

              <Form.Item {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit">
                  Cập nhật
                </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </div>
    );
  }
}

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const mapStateToProps = (state) => {
  return {
    loading: state.loading,
    error: state.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAuth: (username, password) => {
      dispatch(actions.authUpdateAccountInfo(username, password));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
