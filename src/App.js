import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "./store/actions/auth";
import BaseRouter from "./routes";
import "antd/dist/antd.css";

import AppLayout from "./containers/AppLayout";

class App extends React.Component {
  componentDidMount = () => {
    this.props.onTryAutoRegister();
  };

  render() {
    return (
      <div className="App">
        <Router>
          <AppLayout {...this.props}>
            <BaseRouter />
          </AppLayout>
        </Router>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.token !== null,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onTryAutoRegister: () => dispatch(actions.authCheckState()),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
