import React from "react";
import axios from "axios";
import { Row, Col } from "antd";

class ReesList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      estate: {},
      transactionTxt: "",
      price: 0,
    };
  }

  updateFields = () => {
    if (this.state.estate.transaction_type === 1) {
      this.setState({
        transactionTxt: "Bán",
        price: this.state.estate.price_sell,
      });
    } else if (this.state.estate.transaction_type === 2) {
      this.setState({
        transactionTxt: "Thuê",
        price: this.state.estate.price_rent,
      });
    }
  };

  componentDidMount = () => {
    const estateId = this.props.match.params.estateId;
    let apiUrl = `${process.env.REACT_APP_API_GET_A_POST}${estateId}`;
    axios.get(apiUrl).then((response) => {
      this.setState(
        {
          estate: response.data.data,
        },
        () => {
          this.updateFields();
        }
      );
    });
  };
  render() {
    return (
      <div className="rees-detail" style={{ padding: "24px 16px" }}>
        <Row>
          <Col span={8}>
            <div className="detail-left-half">
              <img
                src="/images/rees_avatar.jpg"
                alt="real_estate_avatar"
                style={{ maxWidth: "100%", maxHeight: "100%" }}
              />
            </div>
          </Col>
          <Col span={12}>
            <div
              className="detail-right-half"
              style={{ padding: "24px 0px 0px 24px" }}
            >
              <div className="detail-title" style={{ wordWrap: "break-word" }}>
                <h3>{this.state.estate.title}</h3>
              </div>
              <div className="detail-price">
                <strong>
                  Gía:{" "}
                  <span style={{ color: "tomato" }}>
                    {this.state.price
                      .toFixed(3)
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                  </span>
                </strong>
              </div>
              <div className="detail-trans-type">
                <strong>
                  Loại giao dịch:{" "}
                  <span style={{ color: "tomato" }}>
                    {this.state.transactionTxt}
                  </span>
                </strong>
              </div>
              <div className="detail-content">
                <strong>Chi tiết: </strong>
                <span>{this.state.estate.content}</span>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default ReesList;
