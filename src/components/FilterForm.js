import React from "react";
import { Form } from "antd";
import {
  District,
  City,
  RealEstateType,
  SubmitBtn,
  ReesTransactionType,
} from "../components/ReesInputFormElements";

class FilterForm extends React.Component {
  render() {
    const { styles, cityId } = this.props;
    return (
      <Form
        style={styles}
        {...formItemLayout}
        ref={formRef}
        name="register"
        onFinish={this.props.onFinish}
        scrollToFirstError
      >
        <City
          label={""}
          placeholder={"Tỉnh/Thành"}
          update={(value) => this.props.updateCityId(value)}
        />
        <District
          label={""}
          placeholder={"Quận/Huyện"}
          cityId={cityId}
          update={(value) => this.props.updateDistrictId(value)}
        />
        <RealEstateType
          label={""}
          placeholder={"Loại BĐS"}
          update={(value) => this.props.updateReesType(value)}
        />
        <ReesTransactionType
          label={""}
          placeholder={"Loại giao dịch"}
          update={(value) => this.props.updateTransType(value)}
        />
        <Form.Item {...tailFormItemLayout}>
          <SubmitBtn />
        </Form.Item>
      </Form>
    );
  }
}

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const formRef = React.createRef();

export default FilterForm;
