import React from "react";
import { Row, Col, Select, Button } from "antd";

const { Option } = Select;

const style = { width: "100%" };
const txtStyle = { paddingTop: "5px", textAlign: "right" };

const FilterBar = (props) => {
  return (
    <Row gutter={props.gutter} style={props.style}>
      <Col span={props.span} style={txtStyle}>
        Tỉnh/Thành phố:
      </Col>
      <Col span={props.span}>
        <Select
          placeholder="Chọn Thành Phố"
          optionFilterProp="children"
          onChange={(value) => props.updateCityId(value)}
          onFocus={() => props.getCities()}
          style={style}
        >
          {props.cities.map((city) => (
            <Option key={city.id} value={city.id}>
              {city.name}
            </Option>
          ))}
        </Select>
      </Col>
      <Col span={props.span} style={txtStyle}>
        Quận/Huyện:
      </Col>
      <Col span={props.span}>
        <Select
          placeholder="Chọn Quận/Huyện"
          optionFilterProp="children"
          onChange={(value) => props.updateDistrictId(value)}
          style={style}
        >
          {props.districts.map((district) => (
            <Option key={district.id} value={district.id}>
              {district.name}
            </Option>
          ))}
        </Select>
      </Col>
      <Col span={props.span} style={txtStyle}>
        Loại bđs:
      </Col>
      <Col span={props.span}>
        <Select
          placeholder="Chọn loại bđs"
          optionFilterProp="children"
          onChange={(value) => props.updateReesTypeId(value)}
          style={style}
        >
          {props.reesTypes.map((type) => (
            <Option key={type.id} value={type.id}>
              {type.name}
            </Option>
          ))}
        </Select>
      </Col>
      <Col span={props.span} style={txtStyle}>
        Giao dịch:
      </Col>
      <Col span={props.span}>
        <Select
          placeholder="Chọn giao dịch"
          optionFilterProp="children"
          onChange={(value) => props.updateTransactionId(value)}
          style={style}
        >
          {props.transactions.map((trans) => (
            <Option key={trans.id} value={trans.id}>
              {trans.name}
            </Option>
          ))}
        </Select>
      </Col>
      <Col span={props.span}>
        <Button onClick={() => props.search()} danger>
          Tìm Kiếm
        </Button>
      </Col>
      <Col span={props.span}></Col>
    </Row>
  );
};

export default FilterBar;
