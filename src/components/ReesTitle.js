import React from "react";
import { List, Divider } from "antd";
import Pagination from "react-js-pagination";
import ShowMoreText from "react-show-more-text";
import "../styles/ReesTitle.css";
const NUM_OF_ITEMS = parseInt(process.env.REACT_APP_NUM_OF_ITEMS);

class Rees extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
    };
  }

  onPageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.props.onPageChange(NUM_OF_ITEMS, pageNumber);
  };

  render() {
    return (
      <div className="list-rees">
        <div className="list-rees-header">
          <Divider>Danh sách các bất động sản</Divider>
        </div>
        <div className="list-rees-title">
          <List
            itemLayout="horizontal"
            dataSource={this.props.places}
            renderItem={(item) => (
              <List.Item>
                <List.Item.Meta
                  title={
                    <ShowMoreText lines={1} more="Hiện thêm" less="Ẩn">
                      {item.title}
                    </ShowMoreText>
                  }
                />
              </List.Item>
            )}
          ></List>
        </div>

        <div className="paging">
          <Pagination
            itemClass="page-item"
            linkClass="page-link"
            activePage={this.state.activePage}
            itemsCountPerPage={NUM_OF_ITEMS}
            totalItemsCount={this.props.totalItems}
            pageRangeDisplayed={3}
            onChange={this.onPageChange}
          />
        </div>
      </div>
    );
  }
}

export default Rees;
