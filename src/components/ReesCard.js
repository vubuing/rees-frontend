import React from "react";
import { Row, Col, Card } from "antd";

class ReesCardList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  normalized_price = (post) => {
    if (post.transaction_type === 1) {
      return post.price_sell.toFixed(3).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    return post.price_rent.toFixed(3).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  render() {
    const { gutter, span, posts } = this.props;
    return (
      <Row gutter={gutter}>
        {posts.map((post) => (
          <Col className="gutter-row" span={span} key={post.title}>
            <Card
              title={
                <a
                  style={{ color: "rgba(0, 0, 0, 0.65)" }}
                  href={`/rees-detail/${post.id}`}
                >
                  {post.title}
                </a>
              }
              loading={false}
              style={{ height: "230px" }}
            >
              <p>{post.content.slice(0, 80) + "..."}</p>
              <p>
                <strong>
                  Gía:{" "}
                  <span style={{ color: "tomato" }}>
                    {this.normalized_price(post)}
                  </span>
                </strong>
              </p>
            </Card>
          </Col>
        ))}
      </Row>
    );
  }
}

export default ReesCardList;
