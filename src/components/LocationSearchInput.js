import React from "react";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from "react-places-autocomplete";

class LocationSearchInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { address: "" };
  }

  handleChange = (address) => {
    this.setState({ address });
  };

  handleSelect = async (address) => {
    let geoCode = null;
    await geocodeByAddress(address)
      .then((results) => {
        geoCode = results[0];
      })
      .catch((error) => console.error("Error", error));
    const latLng = await getLatLng(geoCode);
    this.setState({ address: address });
    this.props.updateFromLocationSearch(latLng);
  };

  render() {
    return (
      <PlacesAutocomplete
        value={this.state.address}
        onChange={this.handleChange}
        onSelect={this.handleSelect}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
          <div className="search-box">
            <input
              className="search-txt"
              {...getInputProps({ placeholder: "Nhập địa chỉ..." })}
            />
            <div className="clear" />

            <div style={{ marginTop: "-20px" }}>
              {loading ? <div>...loading</div> : null}
              {suggestions.map((suggestion) => {
                const style = {
                  backgroundColor: suggestion.active ? "#999190" : "#fff",
                  width: "100%",
                  padding: "0px 10px",
                  cursor: "pointer",
                  borderRadius: "4px",
                };
                return (
                  <div {...getSuggestionItemProps(suggestion, { style })}>
                    {suggestion.description}
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </PlacesAutocomplete>
    );
  }
}

export default LocationSearchInput;
