import React from "react";
import { Form, Input, Button, Select } from "antd";
import axios from "axios";

const { Option } = Select;

const AddressNumber = (props) => {
  return (
    <Form.Item
      name="addressNumber"
      label="Số nhà"
      rules={[
        {
          required: true,
          message: "Vui lòng nhập số nhà",
        },
      ]}
    >
      <Input
        placeholder={"268"}
        onChange={(event) => props.update(event.target)}
      />
    </Form.Item>
  );
};

const AddressStreet = (props) => {
  return (
    <Form.Item
      name="addressStreet"
      label="Tên đường"
      rules={[
        {
          required: true,
          message: "Vui lòng nhập tên đường",
        },
      ]}
    >
      <Input
        placeholder={"Lý Thường Kiệt"}
        onChange={(event) => props.update(event.target)}
      />
    </Form.Item>
  );
};

const Ward = (props) => {
  return (
    <Form.Item
      name="ward"
      label="Phường/Xã"
      rules={[
        {
          required: true,
          message: "Vui lòng nhập tên phường/xã",
        },
      ]}
    >
      <Input
        placeholder={"Phường 14"}
        onChange={(event) => props.update(event.target)}
      />
    </Form.Item>
  );
};

class District extends React.Component {
  constructor(props) {
    super(props);
    this.state = { districts: [] };
  }

  componentDidMount = () => {
    this.getDistricts();
  };

  componentDidUpdate = (prevProps) => {
    if (this.props.cityId !== prevProps.cityId) {
      this.getDistricts();
    }
  };

  getDistricts = () => {
    let apiUrl = `${process.env.REACT_APP_API_GET_DISTRICTS_WITHIN_CITY}${this.props.cityId}`;
    axios.get(apiUrl).then((response) => {
      this.setState({ districts: response.data.data });
    });
  };

  render() {
    return (
      <Form.Item
        name="district"
        label={this.props.label}
        rules={[
          {
            required: true,
            message: "Vui lòng chọn tên Quận/Huyện",
          },
        ]}
      >
        <Select
          showSearch
          placeholder={this.props.placeholder}
          optionFilterProp="children"
          onChange={(value) => this.props.update(value)}
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {this.state.districts.map((district) => (
            <Option key={district.id} value={district.id}>
              {district.name}
            </Option>
          ))}
        </Select>
      </Form.Item>
    );
  }
}

class City extends React.Component {
  constructor(props) {
    super(props);
    this.state = { cities: [] };
  }

  componentDidMount = () => {
    // FUTURE DEVELOPMENT
    // let apiUrl = `${process.env.REACT_APP_API_GET_CITIES}`;
    // axios
    //   .get(apiUrl)
    //   .then((reponse) => this.setState({ cities: reponse.data.data }));

    // Now we only process for Ho Chi Minh City
    let apiUrl = `${process.env.REACT_APP_API_GET_CITIES}1`;
    axios
      .get(apiUrl)
      .then((reponse) => this.setState({ cities: [reponse.data.data] }));
  };
  render() {
    return (
      <Form.Item
        name="city"
        label={this.props.label}
        rules={[
          {
            required: true,
            message: "Vui lòng chọn tên Tỉnh/Thành phố",
          },
        ]}
      >
        <Select
          showSearch
          placeholder={this.props.placeholder}
          optionFilterProp="children"
          onChange={(value) => this.props.update(value)}
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {this.state.cities.map((city) => (
            <Option key={city.id} value={city.id}>
              {city.name}
            </Option>
          ))}
        </Select>
      </Form.Item>
    );
  }
}

class RealEstateType extends React.Component {
  constructor(props) {
    super(props);
    this.state = { reesTypes: [] };
  }
  componentDidMount = () => {
    // FUTURE DEVELOPMENT
    // let apiUrl = `${process.env.REACT_APP_API_GET_REALESTATE_TYPES}`;
    // axios.get(apiUrl).then(response => reesTypes = response.data.data);
    let apiUrl = `${process.env.REACT_APP_API_GET_REALESTATE_TYPES}2`;
    axios
      .get(apiUrl)
      .then((response) => this.setState({ reesTypes: [response.data.data] }));
  };
  render() {
    return (
      <Form.Item
        name="realEstateType"
        label={this.props.label}
        rules={[
          {
            required: true,
            message: "Vui lòng chọn loại bất động sản",
          },
        ]}
      >
        <Select
          showSearch
          placeholder={this.props.placeholder}
          optionFilterProp="children"
          onChange={(value) => this.props.update(value)}
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {this.state.reesTypes.map((reesType) => (
            <Option key={reesType.id} value={reesType.id}>
              {reesType.name}
            </Option>
          ))}
        </Select>
      </Form.Item>
    );
  }
}

const NumOfFloor = (props) => {
  return (
    <Form.Item
      name="numOfFloor"
      label="Số tầng"
      rules={[
        {
          required: true,
          message: "Vui lòng nhập số tầng",
        },
      ]}
    >
      <Input
        placeholder={"1"}
        onChange={(event) => props.update(event.target)}
      />
    </Form.Item>
  );
};

class RealEstatePosition extends React.Component {
  constructor(props) {
    super(props);
    this.state = { positions: [] };
  }
  componentDidMount = () => {
    let apiUrl = `${process.env.REACT_APP_API_GET_REALESTATE_POSITION}`;
    axios
      .get(apiUrl)
      .then((response) => this.setState({ positions: response.data.data }));
  };
  render() {
    return (
      <Form.Item
        name="realEstatePosition"
        label="Vị trí BĐS"
        rules={[
          {
            required: true,
            message: "Vui lòng chọn vị trí bất động sản",
          },
        ]}
      >
        <Select
          showSearch
          placeholder="Chọn"
          optionFilterProp="children"
          onChange={(value) => this.props.update(value)}
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {this.state.positions.map((position) => (
            <Option key={position.id} value={position.id}>
              {position.name}
            </Option>
          ))}
        </Select>
      </Form.Item>
    );
  }
}

const Area = (props) => {
  return (
    <Form.Item
      name="area"
      label="Diện tích(m2)"
      rules={[
        {
          required: true,
          message: "Vui lòng nhập diện tích",
        },
      ]}
    >
      <Input
        placeholder={"60"}
        onChange={(event) => props.update(event.target)}
      />
    </Form.Item>
  );
};

const SubmitBtn = () => {
  return (
    <Button type="primary" htmlType="submit" danger>
      Hoàn thành
    </Button>
  );
};

class ReesTransactionType extends React.Component {
  constructor(props) {
    super(props);
    this.state = { transactions: [] };
  }
  componentDidMount = () => {
    let apiUrl = `${process.env.REACT_APP_API_GET_TRANSACTIONS}`;
    axios.get(apiUrl).then((response) => {
      this.setState({ transactions: response.data.data.slice(0, 2) });
    });
  };
  render() {
    return (
      <Form.Item
        name="realEstateTransType"
        label={this.props.label}
        rules={[{ required: true, message: "Chọn loại giao dich" }]}
      >
        <Select
          showSearch
          placeholder={this.props.placeholder}
          optionFilterProp="children"
          onChange={(value) => this.props.update(value)}
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
          {this.state.transactions.map((transaction) => (
            <Option key={transaction.id} value={transaction.id}>
              {transaction.name}
            </Option>
          ))}
        </Select>
      </Form.Item>
    );
  }
}

export {
  AddressNumber,
  AddressStreet,
  Ward,
  District,
  City,
  RealEstateType,
  RealEstatePosition,
  Area,
  NumOfFloor,
  SubmitBtn,
  ReesTransactionType,
};
