import React from "react";
import { Route } from "react-router-dom";
import MapWSearch from "./containers/MapWSearch";
import ReesListWFilters from "./containers/ReesListWFilters";
import Homepage from "./containers/Homepage";
import ReesDetail from "./components/ReesDetail";
import Login from "./containers/Login";
import Register from "./containers/Register";
import Profile from "./containers/Profile";

const BaseRouter = () => {
  return (
    <div className="router" style={{ background: "#ffff" }}>
      <Route exact path="/" component={Homepage} />
      <Route exact path="/map" component={MapWSearch} />
      <Route
        exact
        path="/danh-sach-bat-dong-san"
        component={ReesListWFilters}
      />
      <Route exact path="/rees-detail/:estateId" component={ReesDetail} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/register" component={Register} />
      <Route exact path="/profile" component={Profile} />
    </div>
  );
};

export default BaseRouter;
